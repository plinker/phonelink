<?php
namespace plinker\Phonelink;

use RedBeanPHP\R;

/**
 * Phonelink IPBan
 */
class IPBan {

    /**
     *
     */
    public function __construct(array $config = array(
        'dsn' => 'mysql:host=127.0.0.1;dbname=mydatabase',
        'username' => '',
        'password' => '',
        'freeze' => false,
        'debug' => false,
    )) {
        //hook into RedBean
        new \plinker\Redbean\Redbean($config);
    }

    /**
     *
     */
    private function response($data = null, $status = 200, $errors = array())
    {
        return array(
            'status' => $status,
            'errors' => $errors,
            'data'   => $data
        );
    }

    /**
     *
     */
    public function getBlocked($params = array())
    {
        return $this->response(R::getAll('SELECT * FROM ipban'));
    }

    /**
     *
     */
    public function block($params = array())
    {
        $forbidden = array(
            '0.0.0.0',
            '255.0.0.0',
            '255.255.0.0',
            '255.255.255.0',
        );

        if (in_array($params[0]['ip'], $forbidden)) {
            return $this->response(
                'Forbidden IP block',
                400,
                array('The IP you have requested to blocked is not allowed')
            );
        }

        if (!filter_var($params[0]['ip'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return $this->response(
                'Invalid IP',
                400,
                array('')
            );
        }

        // ...
        $ban = R::findOne('ipban', ' ip = ? AND system_id = ? LIMIT 1', [$params[0]['ip'], $params[0]['system_id']]);

        if (!empty($ban)) {
            return $this->response(
                'IP already blocked',
                200,
                array('IP '.$params[0]['ip'].' has already been blocked on '.$ban->bantime)
            );
        } else {
            $ban = R::dispense('ipban');
            $ban->import($params[0]);
            R::store($ban);
            return $this->response(
                'IP blocked',
                200,
                array('IP '.$params[0]['ip'].' has successfully been blocked')
            );
        }
    }

    /**
     *
     */
    public function unblock($params = array())
    {
        $ban = R::findOne('ipban', ' ip = ? AND system_id = ? LIMIT 1', [$params[0]['ip'], $params[0]['system_id']]);

        if (empty($ban)) {
            return $this->response(
                'IP not blocked',
                200,
                array('IP '.$params[0]['ip'].' has not been blocked')
            );
        } else {
            R::trash($ban);
            return $this->response(
                'IP unblocked',
                200,
                array('IP '.$params[0]['ip'].' has successfully been unblocked')
            );
        }
    }

}
