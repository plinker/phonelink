<?php
namespace plinker\Phonelink;

use RedBeanPHP\R;

/**
 * Phonelink class
 */
class Phonelink {

    /**
     * Construct
     *
     * @param array $config
     */
    public function __construct(array $config = array(
        'dsn' => 'mysql:host=127.0.0.1;dbname=mydatabase',
        'username' => '',
        'password' => '',
        'freeze' => false,
        'debug' => false,
    )) {
        //hook into RedBean
        new \plinker\Redbean\Redbean($config);
    }

    /**
     *
     */
    private function response($data = null, $status = 200, $errors = array())
    {
        return array(
            'status' => $status,
            'errors' => $errors,
            'data'   => $data
        );
    }

    /**
     *
     */
    public function system($params = array())
    {
        $values = $params[0];

        //see if system exist
        $systems = R::count('system', ' system_id = ? ', [$values['system_id']]);

        // delete if needed
        if ($systems > 0 && !empty($values['delete_time']) && is_numeric($values['delete_time'])) {

            R::exec('UPDATE system SET delete_time = NOW() WHERE system_id = ?', [$values['system_id']]);
            //R::exec('DELETE FROM ddi WHERE system_id = ?', [$values['system_id']]);
            //R::exec('DELETE FROM extension WHERE system_id = ?', [$values['system_id']]);
            //R::exec('DELETE FROM member WHERE system_id = ?', [$values['system_id']]);

            $this->phone_log('System '.$values['system_id'].' deleted');

        } else {

            // add or update system -------------------------------------------------
            $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$values['system_id']]);

            // if in reconfigure mode return error
            if ($systems > 0 && $system->reconfigure != 0) {
                return $this->response(
                    'Unprocessable Entity',
                    422,
                    array('system '.$system->system_id.' waiting to be configured')
                );
            }

            // if system not found but attempting to delete return error
            if ($systems == 0 && !empty($values['delete_time']) && is_numeric($values['delete_time'])) {
                return $this->response(
                    'Not Found',
                    404,
                    array('system '.(int) $values['system_id'].' not found')
                );
            }

            // system not found create new one
            if (empty($system)) {
                $system = R::dispense('system');
                $system->added_time =  date('Y-m-d H:i:s');
            }

            $system->system_id = $values['system_id'];
            $system->system_name = $values['system_name'];
            $system->status = $values['status'];
            $system->localcode = $values['localcode'];

            R::store($system);
            // ----------------------------------------------------------------------

            // delete existing ddi --------------------------------------------------
            R::exec('DELETE FROM ddi WHERE system_id = ?', [$values['system_id']]);

            // Add new ddi
            foreach ($values['ddi'] as $row) {
                // check theres data to insert
                if (!empty($row['number']) && !empty($row['destination'])) {
                    $ddi = R::dispense('ddi');
                    $ddi->system_id = $values['system_id'];
                    $ddi->number = $row['number'];
                    $ddi->destination = $row['destination'];
                    $ddi->outbound_clid = $row['outbound_clid'];
                    $ddi->destination_type = $row['destination_type'];

                    R::store($ddi);
                }
            }
            // ----------------------------------------------------------------------

            // delete existing extension --------------------------------------------
            R::exec('DELETE FROM extension WHERE system_id = ?', [$values['system_id']]);
            // Add new extensions
            foreach ($values['extension'] as $row) {
                // check theres data to insert
                $extension = R::dispense('extension');
                $extension->system_id = $values['system_id'];
                $extension->extension_type = $row['extension_type'];
                $extension->ring_type = $row['ring_type'];
                $extension->extension = $row['extension'];
                $extension->secret = $row['secret'];
                $extension->name = $row['name'];
                $extension->timeout = $row['timeout'];
                $extension->destination = $row['destination'];
                $extension->vm_email = $row['vm_email'];
                $extension->vm_pin = $row['vm_pin'];
                $extension->missed_email = $row['missed_email'];
                $extension->message_in = $row['message_in'];
                $extension->message_period = $row['message_period'];
                $extension->announce_place = $row['announce_place'];
                $extension->vm_active = $row['vm_active'];
                $extension->destination_type = $row['destination_type'];

                R::store($extension);
            }
            // ----------------------------------------------------------------------

            // delete existing members --------------------------------------------
            R::exec('DELETE FROM member WHERE system_id = ?', [$values['system_id']]);
            // Add new members
            foreach ($values['member'] as $row) {
                $member = R::dispense('member');
                $member->system_id = $values['system_id'];
                $member->extension = $row['extension'];
                $member->priority = $row['priority'];
                $member->destination = $row['destination'];

                R::store($member);
            }
            // ----------------------------------------------------------------------

            $this->phone_log('System '.$values['system_id'].' updated.');
        }

        // Set system for reconfig
        R::exec('UPDATE system SET reconfigure = 1 WHERE system_id = ?', [$values['system_id']]);

        return $this->response(
            'Success',
            200,
            array('system '.$values['system_id'].' updated, ready for reconfigure')
        );
    }

    /**
     *
     */
    public function blacklist($params = array())
    {
        $values = $params[0];

        if (is_array($values)) {
            foreach ($values as $row) {
                $blacklist  = R::findOne('blacklist', ' system_id = ? AND number = ? LIMIT 1', [$row['system_id'], $row['number']]);

                if (empty($blacklist)) {
                    $blacklist = R::dispense('blacklist');
                }

                $blacklist->system_id = $row['system_id'];
                $blacklist->name = $row['name'];
                $blacklist->number = $row['number'];
                R::store($blacklist);
            }

            $this->phone_log('Blacklist updated.');
            return $this->response(
                'Success',
                200,
                array('blacklist updated')
            );
        } else {
            $this->phone_log('Blacklist failed to update, input data not array.', 'error');
            return $this->response(
                'Bad Request',
                400,
                array('blacklist failed to update, input data not array.')
            );
        }
    }

    /**
     *
     */
    public function crm($params = array())
    {
        $values = $params[0];

        if (is_array($values)) {

            $crm  = R::findOne('crm', ' system_id = ? AND number = ? LIMIT 1', [$values['system_id'], $values['number']]);

            if (empty($crm)) {
                $crm = R::dispense('crm');
                $added = true;
            } else {
                $added = true;
            }

            $crm->cdrId = $values['cdr_id'];
            $crm->contactId = $values['contact_id'];
            $crm->systemId = $values['system_id'];
            $crm->name = $values['name'];
            $crm->number = $values['number'];
            $crm->added_time = $values['added_time'];
            $crm->deleted_time = null;
            $crm->last_called = $values['last_called'];

            R::store($crm);

            if ($added == true) {
                $this->phone_log('CRM new contact added: '.$values['name'].' - '.$values['number']);
                return $this->response(
                    'Success',
                    200,
                    array('crm added')
                );
            } else {
                $this->phone_log('CRM contact '.$crm->id.' updated.');
                return $this->response(
                    'Success',
                    200,
                    array('crm added')
                );
            }
        } else {
            $this->phone_log('CRM failed to update, input data not array.', 'error');
            return $this->response(
                'Bad Request',
                400,
                array('crm failed to update, input data not array.')
            );
        }
    }

    /**
     *
     */
    public function crmDelete($params = array())
    {
        $values = $params[0];

        if (is_array($values)) {

            $crm  = R::findOne('crm', ' system_id = ? AND contact_id = ? LIMIT 1', [$values['system_id'], $values['contact_id']]);

            if (empty($crm)) {
                return $this->response(
                    'Contact not found',
                    400,
                    array('Contact was not found to delete')
                );
            } else {
                $crm->deleted_time = time();
                R::store($crm);
                return $this->response(
                    'Contact Deleted',
                    200,
                    array('Contact was successfully deleted')
                );
            }
        } else {
            $this->phone_log('CRM failed to update, input data not array.', 'error');
            return $this->response(
                'Bad Request',
                400,
                array('crm failed to update, input data not array.')
            );
        }
    }

    /**
     *
     */
    public function speeddial($params = array())
    {
        $values = $params[0];

        if (is_array($values)) {
            foreach ($values as $row) {
                $speeddial  = R::findOne('speeddial', ' system_id = ? AND speeddial = ? LIMIT 1', [$row['system_id'], $row['speeddial']]);

                if (empty($speeddial)) {
                    $speeddial = R::dispense('speeddial');
                }

                $speeddial->system_id = $row['system_id'];
                $speeddial->name = $row['name'];
                $speeddial->speeddial = $row['speeddial'];
                $speeddial->destination = $row['destination'];
                R::store($speeddial);
            }

            $this->phone_log('Speeddial updated.');
            return $this->response(
                'Success',
                200,
                array('speeddial updated')
            );
        } else {
            $this->phone_log('speeddial failed to update, input data not array.', 'error');
            return $this->response(
                'Bad Request',
                400,
                array('speeddial failed to update, input data not array.')
            );
        }
    }

    /**
     *
     */
    public function reconfigure($params = array())
    {
        $systemId = $params[0];

        $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$systemId]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$values['system_id'].' not found')
            );
        } else {

            $system->reconfigure = 1;
            R::store($system);

            return $this->response(
                'Success',
                200,
                array('system updated')
            );
        }
    }

    /**
     * Check if server has system,
     * @return int 1
     */
    public function hasSystem($params = array())
    {
        $systemId = $params[0];

        return R::count('system', ' system_id = ? ', [$params[0]]);
    }

    /**
     * Check system status
     * @return int 1
     */
    public function getStatus($params = array())
    {
        $systemId = $params[0];

        return R::getCell('SELECT status FROM system WHERE system_id = ? LIMIT 1', [$params[0]]);
    }

    /**
     * Set system status
     * @return int 1
     */
    public function setStatus($params = array())
    {
        $systemId = $params[0];
        $status = $params[1];

        $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$systemId]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$values['system_id'].' not found')
            );
        } else {

            $system->status = $status;
            R::store($system);

            return $this->response(
                'Success',
                200,
                array('system updated')
            );
        }
    }

    /**
     * Set system name
     * @return int 1
     */
    public function setName($params = array())
    {
        $systemId = $params[0];
        $name = $params[1];

        $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$systemId]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$values['system_id'].' not found')
            );
        } else {

            $system->system_name = $name;
            R::store($system);

            return $this->response(
                'Success',
                200,
                array('system updated')
            );
        }
    }

    /**
     *
     */
    public function addExtension($params = array())
    {
        $systemId = $params[0];
        $values = $params[1];

        $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$systemId]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$values['system_id'].' not found')
            );
        }

        // extension cleanup
        if ($values['extension_type'] == 0) {
           $values['destination'] = 0;
           $values['destination_type'] = 0;
        }

        // check theres data to insert
        $extension = R::dispense('extension');
        $extension->system_id = $systemId;
        $extension->extension_type = $values['extension_type'];
        $extension->ring_type = 0; //to be added later
        $extension->extension = $values['extension'];
        $extension->secret = $values['secret'];
        $extension->name = $values['name'];
        $extension->timeout = $values['timeout'];
        $extension->destination = $values['destination'];
        $extension->vm_email = $values['vm_email'];
        $extension->vm_pin = $values['vm_pin'];
        $extension->missed_email = $values['missed_email'];
        $extension->message_in = 0; //to be added later
        $extension->message_period = 0;
        $extension->announce_place = 0;
        $extension->vm_active = (!empty($values['vm_active']) ? '1' : '0');
        $extension->destination_type = $values['destination_type'];

        R::store($extension);

        return $this->response(
            'Success',
            200,
            array('extension added')
        );
    }

    /**
     *
     */
    public function addBlacklist($params = array())
    {
        $systemId = $params[0];
        $values = $params[1];

        $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$systemId]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$values['system_id'].' not found')
            );
        }

        $blacklist = R::dispense('blacklist');
        $blacklist->import($values);
        R::store($blacklist);

        return $this->response(
            'Success',
            200,
            array('blacklist added')
        );
    }

    /**
     *
     */
    public function removeExtension($params = array())
    {
        $systemId = $params[0];
        $extension = $params[1];

        $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$systemId]);
        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$values['system_id'].' not found')
            );
        }

        $extension  = R::findOne('extension', ' system_id = ? AND extension = ? LIMIT 1', [$systemId, $extension]);
        if (empty($extension)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('extension '.$extension.' not found on system '.$systemId)
            );
        }

        R::trash($extension);

        return $this->response(
            'Success',
            200,
            array('extension removed')
        );
    }

    /**
     *
     */
    public function updateExtension($params = array())
    {
        $systemId = $params[0];
        $ext = $params[1];
        $values = $params[2];

        //print_r($ext);

        $system  = R::findOne('system', ' system_id = ? LIMIT 1', [$systemId]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$values['system_id'].' not found')
            );
        }

        // extension cleanup
        if ($values['extension_type'] == 0) {
           $values['destination'] = 0;
           $values['destination_type'] = 0;
        }

        $extension  = R::findOne('extension', ' extension = ? AND system_id = ? LIMIT 1 ', [$ext, $systemId]);

        if (empty($extension)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('extension '.$ext.' not found')
            );
        }

        $extension->system_id = $systemId;
        $extension->extension_type = $values['extension_type'];
        $extension->ring_type = 0; //to be added later
        $extension->extension = $values['extension'];
        $extension->secret = $values['secret'];
        $extension->name = $values['name'];
        $extension->timeout = $values['timeout'];
        $extension->destination = $values['destination'];
        $extension->vm_email = $values['vm_email'];
        $extension->vm_pin = $values['vm_pin'];
        $extension->missed_email = $values['missed_email'];
        $extension->message_in = 0; //to be added later
        $extension->message_period = 0;
        $extension->announce_place = 0;
        $extension->vm_active = (!empty($values['vm_active']) ? '1' : '0');
        $extension->destination_type = $values['destination_type'];

        R::store($extension);

        return $this->response(
            'Success',
            200,
            array('extension updated')
        );
    }

    /**
     *
     */
    public function addDDI($params = array())
    {
        //check data
        $data = $params[0];
        if (empty($data)) {
            return $this->response(
                'Bad Request',
                400,
                array('input data empty')
            );
        }

        if (empty($data['system_id'])) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system id required')
            );
        }

        //check system
        $system  = R::findOne('system', ' system_id = ? LIMIT 1 ', [$data['system_id']]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$data['system_id'].' not found')
            );
        }

        //add ddi to database
        $ddi = R::dispense('ddi');
        $ddi->import($data);
        R::store($ddi);

        return $this->response(
            'Success',
            200,
            array('system updated')
        );
    }

    /**
     *
     */
    public function removeDDI($params = array())
    {
        //check system id
        if (empty($params[0])) {
            return $this->response(
                'Bad Request',
                400,
                array('system id required')
            );
        }
        $system_id = $params[0];

        //check number
        if (empty($params[1])) {
             return $this->response(
                'Bad Request',
                400,
                array('phone number required')
            );
        }
        $number = $params[1];

        //check system
        $system  = R::findOne('system', ' system_id = ? LIMIT 1 ', [$system_id]);

        if (empty($system)) {
            return $this->response(
                'Not Found',
                404,
                array('system '.$system_id.' not found')
            );
        }

        //check number
        $ddi  = R::findOne('ddi', ' number = ? AND system_id = ? LIMIT 1 ', [$number, $system_id]);

        if (empty($ddi)) {
            return $this->response(
                'Not Found',
                404,
                array('ddi number '.$number.' for system '.$system_id.' not found')
            );
        }

        //trash it
        R::trash($ddi);

        //reutn success
        return $this->response(
            'Success',
            200,
            array('ddi number removed')
        );
    }

    /**
     *
     */
    public function updateDDI($params = array())
    {
        //check data
        $data = $params[0];
        if (empty($data)) {
            return $this->response(
                'Bad Request',
                400,
                array('input data empty')
            );
        }

        if (empty($data['system_id'])) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system id required')
            );
        }

        //check system
        $system  = R::findOne('system', ' system_id = ? LIMIT 1 ', [$data['system_id']]);

        if (empty($system)) {
            return $this->response(
                'Unprocessable Entity',
                422,
                array('system '.$data['system_id'].' not found')
            );
        }

        //find ddi row
        $ddi  = R::findOne('ddi', ' system_id = ? AND number = ? LIMIT 1 ', [$data['system_id'], $data['number']]);

        if (empty($ddi)) {
            return $this->response(
                'Not Found',
                404,
                array('ddi not found')
            );
        }

        unset($data['id']);
        $ddi->import($data);
        R::store($ddi);

        return $this->response(
            'Success',
            200,
            array('ddi updated')
        );
    }

    /**
     *
     */
    public function phone_log_rotate()
    {
        $log_path = realpath(getcwd().'/../logs');
        $log_file = 'phoneLink.log';
        $new_file = date('Y-m-d').'.log';

        //check rotate not already done
        if (file_exists($log_path.'/'.$new_file)) {
            return;
        }

        //copy log file to new file
        copy(
            $log_path.'/'.$log_file,
            $log_path.'/'.$new_file
        );

        //empty log file
        file_put_contents($log_path.'/'.$log_file, '');

        //log entry
        $this->phone_log('Log rotated to: '.$log_path.'/'.$new_file);
    }

    /**
     *
     */
    public function phone_log($data, $type = 'info')
    {
        $log_path = realpath(getcwd().'/../logs');
        $log_file = 'phoneLink.log';

        //check log folder is there
        if (!file_exists($log_path.'/')) {
            mkdir($log_path.'/', 0777, true);
        }

        //check log file is there
        if (!file_exists($log_path.'/'.$log_file)) {
            touch($log_path.'/'.$log_file);
        }

        //write to log
        file_put_contents(
            $log_path.'/'.$log_file, date(DATE_RFC2822).' ['.$type.'] '.$data.PHP_EOL,
            FILE_APPEND
        );
    }

}
